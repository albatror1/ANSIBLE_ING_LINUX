# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}



provider "openstack" {
  user_name   = "admin"
  tenant_name = "admin"
  domain_name = "Default"
  password    = "44f214ee8ac143a9"
  auth_url    = "http://192.168.0.24:5000"
}

resource "openstack_compute_instance_v2" "crear_instancia" {
  name        = "awx-0003"
  flavor_id   = "73daced9-830e-4852-88d7-0af658c4bff6"
  image_id    = "08feec61-72a2-441a-b1fb-6e307abb6ad1"
  key_pair    = "rojeda"
  security_groups = ["5655d5fa-6b19-40eb-9783-c12ea2a096b3"]

  network {
    uuid = "8a783c16-a638-49dc-a0f2-dae50f83ea6e"
  }
}
