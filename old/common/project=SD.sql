project = SD 
AND issuetype = "Service Request" 
AND status not in (Resolved, Closed, Canceled, Cancelled) AND resolution = Unresolved 
AND (assignee in (membersOf("Linux - ING - LATAM"), EMPTY) OR assignee in (membersOf("Oke - ING - LATAM"), EMPTY)) 
AND ("Owner group" = "Linux - ING - LATAM" OR "Owner group" = "Oke - ING - LATAM") 
ORDER BY created DESC




project = CH AND issuetype in (standardIssueTypes(), subTaskIssueTypes()) 
AND status in ("Assessment revision", AUTH, CORRECTION, Implement, "In Assesment", "In Progress", Pending, Review, "Schedule Extension", "Technical assessment", "To Do") 
AND assignee in (membersOf("Oracle - OS - LATAM")) 
ORDER BY cf[12545] ASC, created DESC



project = CH AND issuetype in (standardIssueTypes(), subTaskIssueTypes()) 
AND status not in (Resolved, Closed, Canceled, Cancelled) AND resolution = Unresolved 
AND assignee in (membersOf("Oracle - OS - LATAM")) 
ORDER BY cf[12545] ASC, created DESC


project = SD AND issuetype in ("Service Request", "Service Request with Approvals", "Service Request with CISO Approvals") AND "Owner group" in ("Oracle - OS - LATAM") 
AND status not in (Resolved, Closed, Canceled, Cancelled) 
AND resolution = Done AND "Close Date[Time stamp]" >= -4w
ORDER BY priority DESC, created ASC


project = SD AND assignee in (membersOf("Oracle - OS - LATAM")) 
AND issuetype = Incident 
AND status in (Resolved, Closed, CLOSED, Canceled, CANCELED, Cancelled, CANCELLED)

and "Close Date[Time stamp]" >= -3w AND "Close Date[Time stamp]" <= 3w